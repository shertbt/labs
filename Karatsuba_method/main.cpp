﻿#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#define NUM_BITS (sizeof(long long) * 8)-1
using namespace std;
int get_length(long long value)
{
	int msb;
	for (msb = NUM_BITS; (value & ((unsigned long long) 1 << msb)) == 0; msb--);
	msb++;
	return msb;
}
unsigned long long karatsuba_mul( long long u, long long v)
{
	unsigned long long result = 0;
	int n = 0;
	int uLength = get_length(u);
	int vLength = get_length(v);
	if (uLength > vLength) {
		n = uLength / 2 + uLength % 2;
	}
	else {
		n = vLength / 2 + vLength % 2;
	}
	unsigned long long u0, u1, v0, v1, A, B, C;
	unsigned long long mask = ((unsigned long long)1 << n) - 1;
	u0 = u & mask;
	v0 = v & mask;
	u1 = u >> n;
	v1 = v >> n;
	A = u1 * v1;
	B = u0 * v0;
	C = A + B + u1 * v0 + v1 * u0;
	unsigned long long tmp = (unsigned long long)1 << (2 * n);
	unsigned long long tmp2 = (unsigned long long)1 << n;
	result = B + A * tmp + (C - A - B) * tmp2;
	return result;

}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);
	
	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char xc[100] = { 0 };
		char yc[100] = { 0 };
		char m = '\0';
		long long x, y;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		iss1 >> m;

		if (m != '\0') {
			m = '\0';
			continue;
		}
		if (!checker(xc) || !checker(yc)) continue;
		if (xc[0] == '\0' || yc[0] == '\0') continue;
		x = atoll(xc);
		y = atoll(yc);
		if ((x > 0) && (y > 0)) {
			try {
				unsigned long long result = karatsuba_mul(x, y);
				file << result << endl;
			}
			catch (invalid_argument & e) {
				cerr << e.what() << endl;
				continue;
			}
		}
		else if (x == 0 || y == 0) {
			unsigned long long result = 0;
			file << result << endl;
		}
	}
	input.close();
	file.close();
	return 1;
}
