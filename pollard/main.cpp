#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <cmath>
#include <map>
#include <set>
#include <vector>
using namespace std;

int gcd(int a, int b) { //���
	if (b == 0)
		return a;
	else
		return gcd(b, a % b);
}
vector <int> create_primes(int n) { //������� ��� ������� ����� ������ n

	vector<int> prime_numbers;
	vector<bool> prime(n + 1, true);
	prime[0] = prime[1] = false;
	for (int i = 2; i <= n; ++i)
	{
		if (prime[i])
		{
			if (i * 1ll * i <= n)
			{
				for (int j = i * i; j <= n; j += i)
				{
					prime[j] = false;
				}
			}
		}
	}
	for (int p = 2; p <= n; p++)
	{
		if (prime[p]) {
			prime_numbers.push_back(p);
		}
	}
	return prime_numbers;
};
bool isprime(int n) {
	int s = sqrt(n);
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (n % primes[i] == 0) {
			return false;
		}
	}
	return true;
};
int R2L(int x, int y, int m)
{
	int q = x;
	int z;
	if ((y & 1) == 0) {
		z = 1;
	}
	else z = x;

	y = y >> 1;
	while (y > 0)
	{

		q = (q * q) % m;
		if ((y & 1) == 1)
		{
			z = (z * q) % m;
		}
		y = y >> 1;
	}
	return z;
}
int find_generator(int p) {
	int n = p - 1;
	int s = sqrt(n);
	vector<int> divisors;
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (n % primes[i] == 0) {
			divisors.push_back(primes[i]);
		}
	}

	for (int i = 2; i < p; i++)
	{
		bool is_generator = true;
		for (int d : divisors) {
			int k = n / d;
			if (R2L(i, k, p) == 1) {
				is_generator = false;
				break;
			}
		}
		if (is_generator) return i;
	}
};

struct XYZ {
	int x;
	int z;
	int y;
	XYZ(int a, int b, int c) {
		x = a;
		y = b;
		z = c;
	}
};

int new_x(int x, int a, int g, int n) {
	if (x % 3 == 1) {
		return (a * x) % n;
	}
	else if (x % 3 == 2) {
		return (x * x) % n;
	}
	else {
		return (g * x) % n;
	}
};

int new_y(int y, int x, int n) {
	if (x % 3 == 1) {
		return (y + 1) % (n - 1);
	}
	else if (x % 3 == 2) {
		return (2 * y) % (n - 1);
	}
	else {
		return y % (n - 1);
	}
};

int new_z(int z, int x, int n) {
	if (x % 3 == 1) {
		return z % (n - 1);
	}
	else if (x % 3 == 2) {
		return (2 * z) % (n - 1);
	}
	else {
		return (z + 1) % (n - 1);
	}
};

XYZ get_XYZ(XYZ xyz, int a,int g, int n ) {
	XYZ result(0, 0, 0);
	result.x = new_x(xyz.x, a, g, n);
	result.y = new_y(xyz.y, xyz.x, n);
	result.z = new_z(xyz.z, xyz.x, n);

	return result;
};

int pollard(int n, int a) {
	XYZ k1(1, 0, 0);
	XYZ k2(1, 0, 0);
	int g = find_generator(n);
	do {
		k1 = get_XYZ(k1, a, g, n);
		k2 = get_XYZ(get_XYZ(k2, a, g, n), a, g, n);
	} while (k1.x != k2.x);
	if (k1.y == k2.y) {
		throw "Error";
	}
	else {
		int r = (k1.y - k2.y) % (n - 1);
		if (r < 0) {
			r = r + n - 1;
		}
		int d = gcd(r, n - 1);
		vector<int> solutions;
		int left;
		int right = (k2.z - k1.z) % (n - 1);
		if (right < 0) {
			right = right + n - 1;
		}
		for (int i = 1; i < n; i++) {
			left = (r * i) % (n - 1);
			if (left == right) {
				solutions.push_back(i);
			}
		}
		for (int j = 0; j < d; j++) {
			if (R2L(g, solutions[j], n) == a) {
				return solutions[j];
			}
		}
	}
};

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char nc[100] = { 0 };
		char ac[100] = { 0 };
		char e = '\0';
		int n,a;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> ac;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc) || !checker(ac)) continue;
		if (nc[0] == '\0' || ac[0] == '\0') continue;
		n = atoi(nc);
		a = atoi(ac);
		if (n <= 0 || a <= 0 || a >= n || !isprime(n)) continue;
		try {
			int res = pollard(n, a);
			file << res << endl;
		}
		catch (const char* msg) {
			continue;
		}
	}
	input.close();
	file.close();
	return 1;
}