﻿#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <sstream>
#define NUM_BITS (sizeof(long long) * 8)-1
using namespace std;
int get_length(long long value)
{
	int msb;
	for (msb = NUM_BITS; (value & ((unsigned long long) 1 << msb)) == 0; msb--);
	msb++;
	msb = msb / 2 + msb % 2;
	return msb;
}
unsigned long long barett(long long x, long long m)
{
	int n = get_length(x);
	int k = get_length(m);
	if (n > 2 * k) {
		throw invalid_argument("Argument too large");
	}
	unsigned long long z = ((unsigned long long) 1 << (4 * k)) / m;
	unsigned long long xq = x >>(2*(k-1));
	unsigned long long tmp = (unsigned long long) 1<<(2*(k + 1));
	unsigned long long q = z * xq / tmp;
	unsigned long long r1 = x & (tmp - 1);
	unsigned long long r2 = (q * m) & (tmp - 1);
	unsigned long long r = 0;
	if (r1 >= r2) {
		r = r1 - r2;
	}
	else {
		r=tmp + r1 - r2;
	}
	while (r >= m) {
		r = r - m;
	}
	return r;
}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);
	
	if (!input.is_open() || !file.is_open())
		return 0;

	while (!input.eof())
	{
		char xc[100] = { 0 };
		char yc[100] = { 0 };
		char m='\0';
		long long x, y;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		if (line.empty()) continue;
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		iss1 >> m;
		if (m != '\0') {
			m = '\0';
			continue;
		}
		if (!checker(xc) || !checker(yc)) continue;
		x = atoll(xc);
		y = atoll(yc);
		if ((x > 0) && (y > 0)) {
			try {
				unsigned long long result = barett(x, y);
				file << result << endl;
			} 
			catch (invalid_argument &e) {
				//cerr << e.what() << endl;
				continue;
			}
		}
		else if (x == 0) {
			unsigned long long result = 0;
			file << result << endl;
		}
	}
	input.close();
	file.close();
	return 1;
}