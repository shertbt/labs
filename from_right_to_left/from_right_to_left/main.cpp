#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
using namespace std;
long long R2L(long long x, long long y, long long m)
{
	long long q = x;
	long long z;
	if ((y & 1) == 0) {
		z = 1;
	}
	else z = x;

	y=y >> 1;
	while(y > 0)
	{

		q = (q * q) % m;
		if ((y & 1) == 1)
		{
			z = (z * q) % m;
		}
		y=y >> 1;
	}
	return z;
}
bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}
int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream output(outfilename, std::ios::out);
	char xc[100];
	char yc[100];
	char mc[100];
	long long x, y, m;
	if (!input.is_open() || !output.is_open()) 
		return 0;
	while (!input.eof())
	{
		
		string line;
		getline(input, line);
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		iss1 >> mc;
		if (!checker(xc) || !checker(yc) || !checker(mc)) continue;
		if (xc[0] == NULL || yc[0] == NULL || mc[0] == NULL) continue;
		x = atoll(xc);
		y = atoll(yc);
		m = atoll(mc);

		if ((x > 0) && (y > 0) && (m > 0)) {
			long long result = R2L(x, y, m);
			output << result << endl;
		}
		if (x == 0) {
			long long result = 0;
			output << result << endl;
		}
		if (y == 0) {
			long long result = 1;
			output << result << endl;
		}
	}
	input.close();
	output.close();
	return 1;
}