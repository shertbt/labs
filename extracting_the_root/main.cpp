﻿#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <sstream>
#define NUM_BITS (sizeof(long long) * 8)-1
using namespace std;
unsigned long long R2L(unsigned long long x, unsigned long long y)
{
	unsigned long long q = x;
	unsigned long long z;
	if ((y & 1) == 0) {
		z = 1;
	}
	else z = x;

	y = y >> 1;
	while (y > 0)
	{

		q = (q * q);
		if ((y & 1) == 1)
		{
			z = (z * q);
		}
		y = y >> 1;
	}
	return z;
}
unsigned long long extracting_the_root(long long a, long long n)
{
	unsigned long long a1=a;
	unsigned long long a0 = a;
	do {
		a0 = a1;
		a1 = (a /R2L(a0, n - 1) + (n - 1) * a0) / n;
	} while (a0 > a1);
	return a0;
}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);
	
	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{
		char xc[100] = { 0 };
		char yc[100] = { 0 };
		long long x, y;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		
		if (!checker(xc) || !checker(yc)) continue;
		if (xc[0] == '\0' || yc[0] == '\0') continue;
		x = atoll(xc);
		y = atoll(yc);
		if ((x > 0) && (y > 0)) {
			unsigned long long result = extracting_the_root(x, y);
			file << result << endl;
			
		}
	}

	input.close();
	file.close();
	return 1;
}