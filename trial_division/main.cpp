#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <vector>
#define ull unsigned long long
using namespace std;

struct division_result {
	vector<int> factors;
	int flag;
	ull remainder;
};
division_result trial_division(ull n, ull b) {
	int k = 1;
	int d = 3;
	division_result p;
	p.flag = 0;
	p.remainder = 0;
	if (b < d) {
		p.flag = 1;
		p.remainder = n;
		return p;
	}
	while (n > 1) {

	l1:
		ull q = n / d;
		if (n % d == 0) {
			(p.factors).push_back(d);
			n = q;
			continue;
		}
		if (d < q) {
			k++;
			if (k == 2) {
				d = 5;
			}
			else if (k % 2 == 0) {
				d = d + 4;
			}
			else if (k % 2 != 0) {
				d = d + 2;
			}
			if (d > b) {
				p.flag = 1;
				p.remainder = n;
				return p;
			}
			goto l1;
		}
		else {
			(p.factors).push_back(n);
			p.remainder = 1;
			return p;
		}
	}
	p.flag = 0;
	p.remainder = 1;
	return p;
}
bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char nc[100] = { 0 };
		char bc[100] = { 0 };
		char e = '\0';
		ull n, b;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> bc;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc) || !checker(bc)) continue;
		if (nc[0] == '\0' || bc[0] == '\0') continue;
		n = atoll(nc);
		b = atoll(bc);
		if (n <= 0 || b <= 0) continue;
		while ((n & (ull)1) == 0) {
			file << "2 ";
			n = n >> 1;
		}
		division_result p = trial_division(n, b);
		//if ((p.factors).size() == 1 && (n == (p.factors[0]))) continue;
		
		for (int i = 0; i < (p.factors).size(); i++) {
			file << p.factors[i] << " ";

		}
		file << p.remainder << " " << p.flag << endl;

	}
	input.close();
	file.close();
	return 1;
}