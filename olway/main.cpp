#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <cmath>
#define ull unsigned long long
using namespace std;

ull olway(ull n) {
	ull d=2*((ull)pow(n,1.0/3))+1;
	ull s=(ull)sqrt(n);
	ull r1 = n % d;
	ull r2 = n % (d - 2);
	ull q = 4 * ((ull)(n / (d - 2)) - (ull)(n / d));
 l1:
	d = d + 2;
	if (d > s) { 
		return 0;
	}
	ull r = 2 * r1 - r2 + q;
	if (r < 0) {
		r += d;
		q += 4;
	}
	while (r >= d) {
		r = r - d;
		q = q - 4;
	}
	if (r == 0) {
		return d;
	}
	else{
		r2 = r1;
		r1 = r;
		goto l1;
	}
}


bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char nc[100] = { 0 };
		char e = '\0';
		ull n, b;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc) ) continue;
		if (nc[0] == '\0' ) continue;
		n = atoll(nc);
		if (n <= 0) continue;
		while ((n & (ull)1) == 0) {
			//file << "2 ";
			n = n >> 1;
		}
		ull p = olway(n);
		if (p == 0) continue;
		else file << p << endl;

	}
	input.close();
	file.close();
	return 1;
}