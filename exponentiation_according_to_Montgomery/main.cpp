﻿#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#define NUM_BITS (sizeof(long long) * 8)-1
#define ull unsigned long long
using namespace std;

int get_length(ull value, int a)
{	
	int msb;
	for (msb = NUM_BITS; (value & ((ull) 1 << msb)) == 0; msb--);
	msb++;
	msb = msb / a + msb % a;
	return msb;
}

struct gcdstruct {
	long long  d;
	long long x;
	long long y;
};

gcdstruct Extended_Euclid(ull a, ull b)
{
	gcdstruct aa, bb;
	if (b == 0) {
		aa.d = a;
		aa.x = 1;
		aa.y = 0;
		return aa;
	}
	else {
		bb = Extended_Euclid(b, a % b);
		aa.d = bb.d;
		aa.x = bb.y;
		aa.y = bb.x - bb.y * (a / b);
	}
	return aa;
}

long long get_inverse(ull num, ull mod) {
	
	gcdstruct aa;
	aa = Extended_Euclid(num, mod);
	return aa.x;

}


ull Montgomery_transformation(ull x, ull b, ull m) {

	int a;
	for (a = 0; ((ull)b & ((ull)1 << a)) == 0; a++);
	int k = get_length(m, a);
	ull bk = (ull) 1 << (k*a);
	ull z;
	if (x > m * bk) {
		throw invalid_argument("Argument too large");
	}
	else {
			long long inverse = -get_inverse(m & (b - 1), b);
			ull m_i = inverse % b;
			z = x;
			for (int i = 0; i < k; i++) {
				ull u = (((z >> (a * i))& (b - 1))* m_i) % b;
				z = z + u * m * ((ull)1 << (i * a));
			}
			z = z / bk;
			if (z >= m) {
				z = z - m;
			}
		 }
	return z;
}

ull Montgomery_multiplication(ull x, ull y, ull b, ull m) {
	ull z;
	if (x > m || y > m) {
		throw invalid_argument("Argument too large");
	}
	else {
			int a;
			for (a = 0; ( (ull)b & ((ull)1 << a) ) == 0; a++);
			int k = get_length(m, a);
			long long inverse = -get_inverse(m & (b - 1), b);
			ull m_i = inverse % b;
			z = 0;
			for (int i = 0; i < k; i++) {
				ull u = (((z & (b - 1)) + (y & (b - 1)) * ((x >> (a * i))& (b - 1)))* m_i) % b;
				z = (z + u * m + y * ((x >> (a * i))& (b - 1))) / b;
			}
			if (z > m) z = z - m;
	}
	return z;
}

ull Montgomery_exponentiation(ull x, ull y, ull b, ull m) {

	int a;
	for (a = 0; ((ull)b & ((ull)1 << a)) == 0; a++);
	int k = get_length(m, a);
	long long inverse = -get_inverse(m & (b - 1), b);
	ull m_i = inverse % b;
	ull R2 = ((ull)1 << (2 * k * a)) % m;
	ull x_i = Montgomery_multiplication(x, R2, b, m);
	ull z = x_i;
	int n = get_length(y, 1);
	for (int i = n - 2; i >= 0; i--) {
		z = Montgomery_multiplication(z, z, b, m);
		if (( (y >> i) & (ull) 1) == 1) {
			z = Montgomery_multiplication(z, x_i, b, m);
		}
	}
	z = Montgomery_transformation(z, b, m);
	return z;
}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char xc[100] = { 0 };
		char yc[100] = { 0 };
		char mc[100] = { 0 };
		char bc[100] = { 0 };
		char e = '\0';
		ull x, y, m, b;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		iss1 >> mc;
		iss1 >> bc;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(xc) || !checker(yc) || !checker(mc) || !checker(bc)) continue;
		if (xc[0] == '\0' || yc[0] == '\0' || mc[0] == '\0' || bc[0] == '\0') continue;
		b = atoll(bc);
		if (b % 2 == 0) {
			x = atoll(xc);
			y = atoll(yc);
			m = atoll(mc);

 			if ((x > 0) && (y > 0) && (m > 0) && (b > 0)) {
				try {
					ull result = Montgomery_exponentiation(x, y, b, m);
					file << result << endl;
				}
				catch (invalid_argument & e) {
					cerr << e.what() << endl;
					continue;
				}
			}
			else if (x == 0 || y == 0) {
				ull result = 0;
				file << result << endl;
			}
		}
		else continue;
	}
	input.close();
	file.close();
	return 1;
}

