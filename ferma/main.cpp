#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <cmath>
#include <map>
#include <set>
#include <vector>
using namespace std;

struct divisors {
	int a;
	int b;
};

map< int, set<int> > get_sets(vector<int> M) {
	map< int, set<int> > Q;
	set<int> tmp;
	for (int m : M) {
		for (int i = 1; i < m; i++) {
			int k = i * i % m;
			tmp.insert(k);
		}
		Q[m] = tmp;
	}
	return Q;
}

divisors ferma(int n) {
	int x = (int)sqrt(n);
	divisors d;
	d.a = 0;
	d.b = 0;
	if (x * x == n) {
		d.a = x;
		d.b = x;
		return d;
	}
	int z ,y = 0;
	int B =( n + 9 )/ 6;
	vector<int> M = { 3,4,5,7 };
	map< int, set<int> > Q=get_sets(M);
	set<int>::iterator it;
	int k = 4; 
	int s[4][7];
	int r[4];
	bool flag = false;
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < M[i]; j++) {
			int tmp = (j * j - n) %  M[i];
			if (tmp < 0) tmp = tmp + M[i];
			it = Q[M[i]].find(tmp);
			if ((tmp == 0) || (it != Q[M[i]].end())) {
				s[i][j] = 1;
			}
			else {
				s[i][j] = 0;
			}
		}
	}
	x++;
	for (int i = 0; i < k; i++) {
		r[i] = x % M[i];
	}
l1:
	flag = false;
	for (int i = 0; i < k; i++) {
		if (s[i][r[i]] != 1) flag = true;
	}
	if (flag == false) {
		z = x * x - n;
		y = (int)sqrt(z);
		if (y * y == z) {
			d.a = x + y;
			d.b = x - y;
			return d;
		}
	}
	x++;
	if (x > B) {
		d.a = 0;
		d.b = 0;
		return d;
	}
	else{
		for (int i = 0; i < k; i++) {
			r[i] = (r[i]+1) % M[i];
		}
		goto l1;
	}

}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
}

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{

		char nc[100] = { 0 };
		char e = '\0';
		int n;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc)) continue;
		if (nc[0] == '\0') continue;
		n = atoi(nc);
		if (n <= 0) continue;
		while ((n & (int)1) == 0) {
			//file << "2 ";
			n = n >> 1;
		}
		divisors p = ferma(n);
		if ( (p.a != 0 && p.b != 0 ) && (n != p.a && n != p.b) )
		{ file << p.a << " " << p.b << endl; }

	}
	input.close();
	file.close();
	return 1;
}