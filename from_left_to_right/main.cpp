#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#define NUM_BITS (sizeof(long long) * 8) - 1
using namespace std;
long long L2R(long long x, long long y, long long m)
{
	int msb;
	for (msb = NUM_BITS; (y & ((unsigned long long)1 << msb)) == 0; msb--);
	long long z = x;
	for (int bit = msb - 1; bit >= 0; bit--)
	{

		z = (z * z) % m;
		if (((y >> bit)& (long long)1) == 1)
		{
			z = (z * x) % m;
		}
	}
	return z;
}

bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9' ) {
			return false;
		}
	}
	return true;
}
int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream output(outfilename, std::ios::out);
	long long x, y, m;
	char xc[100] ;
	char yc[100] ;
	char mc[100] ;
	if (!input.is_open() || !output.is_open())
		return 0;
	while (!input.eof())
	{
		string line;
		getline(input, line);
		istringstream iss1(line);
		iss1 >> xc;
		iss1 >> yc;
		iss1 >> mc;
		if (!checker(xc) || !checker(yc)|| !checker(mc)) continue;
		if (xc[0] == NULL || yc[0] == NULL || mc[0] == NULL) continue;
		x = atoll(xc);
		y = atoll(yc);
		m = atoll(mc);
		
		if ((x > 0) && (y > 0) && (m > 0)) {
			long long result = L2R(x, y, m);
			output << result << endl;
		}
		if (x == 0) {
			long long result = 0;
			output << result << endl;
		}
		if (y == 0) {
			long long result = 1;
			output << result << endl;
		}
	}

	input.close();
	output.close();
	return 1;
}