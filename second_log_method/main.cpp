#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include <cstring>
#include <sstream>
#include <map>
#include <cmath>
using namespace std;

int gcd(int a, int b) { //���
	if (b == 0)
		return a;
	else
		return gcd(b, a % b);
}
struct gcdstruct {
	int d;
	int x;
	int y;
};

gcdstruct Extended_Euclid(int a, int b)
{
	gcdstruct aa, bb;
	if (b == 0) {
		aa.d = a;
		aa.x = 1;
		aa.y = 0;
		return aa;
	}
	else {
		bb = Extended_Euclid(b, a % b);
		aa.d = bb.d;
		aa.x = bb.y;
		aa.y = bb.x - bb.y * (a / b);
	}
	return aa;
};

int get_inverse(int num, int mod) {

	gcdstruct aa;
	aa = Extended_Euclid(num, mod);
	while (aa.x < 0) {
		aa.x += mod;
	}
	return aa.x;

};
vector <int> create_primes(int n) { //������� ��� ������� ����� ������ n

	vector<int> prime_numbers;
	vector<bool> prime(n + 1, true);
	prime[0] = prime[1] = false;
	for (int i = 2; i <= n; ++i)
	{
		if (prime[i])
		{
			if (i * 1ll * i <= n)
			{
				for (int j = i * i; j <= n; j += i)
				{
					prime[j] = false;
				}
			}
		}
	}
	for (int p = 2; p <= n; p++)
	{
		if (prime[p]) {
			prime_numbers.push_back(p);
		}
	}
	return prime_numbers;
};
vector<int> create_factor_base(int k) { //��������� ����(k-���������� ����� � ��������� ����)
	vector<int> prime_numbers;
	int size = k;
	int* num = new int[size];
	for (int i = 0; i < size; i++) {
		num[i] = i;
	}
	prime_numbers.push_back(2);
	int j = 0;
	while (j < k - 1) {
		int p = prime_numbers[j++]; // ���������� ������� ������� �����
		for (int i = p * 2; i < size; i += p)
			num[i] = 0; // �������� ��� ������� ��� ����� � �������
		while (num[p + 1] == 0)
			p++; // ���� ��������� ��������� �����
		if (p + 1 >= size) { // ���� ������ �� �������, ��������� ������
			int* tmp = new int[size * 2];
			for (int k = 0; k < size; k++)
				tmp[k] = num[k];

			delete[] num;
			size *= 2;
			num = tmp;

			for (int l = size / 2; l < size; l++)
				num[l] = l;
			j = 0;
		}
		else {
			//j++;
			prime_numbers.push_back(p + 1); // ���������� ����� ������� �����
		}
	}
	delete[]num;
	return prime_numbers;
};

bool isprime(int n) {
	int s = sqrt(n);
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (n % primes[i] == 0) {
			return false;
		}
	}
	return true;
};
bool isPower(int n)
{
	int s = sqrt(n);
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (trunc(log(n) / log(primes[i])) == (log(n) / log(primes[i]))) {
			return true;
		}
	}
	return false;
};
bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
};

int R2L(int x, int y, int m)
{
	int q = x;
	int z;
	if ((y & 1) == 0) {
		z = 1;
	}
	else z = x;

	y = y >> 1;
	while (y > 0)
	{

		q = (q * q) % m;
		if ((y & 1) == 1)
		{
			z = (z * q) % m;
		}
		y = y >> 1;
	}
	return z;
}
int find_generator(int p) {
	int n = p - 1;
	int s = (int)sqrt(n);
	vector<int> divisors;
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (n % primes[i] == 0) {
			divisors.push_back(primes[i]);
		}
	}

	for (int i = 2; i < p; i++)
	{
		bool is_generator = true;
		for (int d : divisors) {
			int k = n / d;
			if (R2L(i, k, p) == 1) {
				is_generator = false;
				break;
			}
		}
		if (is_generator) return i;
	}
};
map<int, int> decomposition(int n) {
	int s = sqrt(n);
	vector<int> primes = create_primes(n);
	map<int, int> factors;
	for (int i = 0; i < primes.size(); i++) {
		while (n % primes[i] == 0) {
			factors[primes[i]]++;
			n /= primes[i];
		}
	}
	return factors;
};
int power(int g, int y, int n) {
	int result = 0;
	result = get_inverse(R2L(g, y, n), n);
	return result;
};
int ChiResTheorem(int n, map<int, int> nums, vector<int> x) {
	vector<int> b;
	int q, z = 0;
	for (auto it = nums.begin(); it != nums.end(); it++)
	{
		b.push_back(pow(it->first, it->second));
	}
	
	auto size = x.size();
	for (int i = 0; i < size; i++)
	{
		int M_i = n / b[i];
		q = get_inverse(M_i, b[i]);
		z += x[i] *M_i * q;
		
	}
	if (z > n) {
		z %= n;
	}
	return z;
};

int pohlig_hellman(int n, int a) {
	int result = 0;
	int p = n - 1;
	int g = find_generator(n);
	map<int, int> factors = decomposition(p);
	auto size = factors.size();
	vector< map<int,int> > r;
	vector<int> x;
	int y;
	int p_i;
	for (auto it = factors.begin(); it != factors.end(); it++) {
		p_i = it->first;
		y = R2L(g, p / p_i, n);
		map<int, int> r_i;
		for (int j = 0; j < p_i; j++) {
			int r_j = R2L(y, j, n);
			r_i[r_j] = j;
		}
		r.push_back(r_i);
	}
	int i = 0;
	map<int, int>::iterator itr;
	for (auto it = factors.begin(); it != factors.end(); it++) {
		p_i = it->first;
		int e = it->second;
		int b = R2L(a, n/p_i , n);
		map<int, int> r_i=r[i];
		itr = r_i.find(b);
		int y = itr->second;
		for (int j = 1; j < e; j++) {
			b = R2L(a * power(g, y, n), n / pow(p_i, j + 1), n);
			itr = r_i.find(b);
			int x_j = itr->second;
			y += x_j * pow(p_i, j);
		}
		x.push_back(y);
		i++;
	}
	result=ChiResTheorem(p, factors, x);
	return result;
};

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{
		char nc[100] = { 0 };
		char ac[100] = { 0 };
		char e = '\0';
		int n, a;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> ac;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc) || !checker(ac)) continue;
		if (nc[0] == '\0' || ac[0] == '\0') continue;
		n = atoi(nc);
		a = atoi(ac);
		if (n <= 0 || a <= 0 || a >= n || !isprime(n)) continue;
		try {
			int res = pohlig_hellman(n, a);
			file << res << endl;
		}
		catch (const char* msg) {
			continue;
		}

	}
	input.close();
	file.close();
	return 1;
}