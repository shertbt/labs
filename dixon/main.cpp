﻿#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include <cstring>
#include <sstream>
using namespace std;

int gcd(int a, int b) { //НОД
	if (b == 0)
		return a;
	else
		return gcd(b, a % b);
}
vector <int> create_primes(int n) { //находит все простые числа меньше n

	vector<int> prime_numbers;
	vector<bool> prime(n + 1, true);
	prime[0] = prime[1] = false;
	for (int i = 2; i <= n; ++i)
	{
		if (prime[i])
		{
			if (i * 1ll * i <= n)
			{
				for (int j = i * i; j <= n; j += i)
				{
					prime[j] = false;
				}
			}
		}
	}
	for (int p = 2; p <= n; p++)
	{
		if (prime[p]) {
			prime_numbers.push_back(p);
		}
	}
	return prime_numbers;
};
vector<int> create_factor_base(int k) { //факторная база(k-количество чисел в факторной базе)
	vector<int> prime_numbers;
	int size = k;
	int* num=new int[size];
	for (int i = 0; i < size; i++) {
		num[i]=i;
	}
	prime_numbers.push_back(2);
	int j = 0;
	while (j<k-1) {
		int p = prime_numbers[j++]; // запоминаем текущее простое число
		for (int i = p * 2; i < size; i += p)
			num[i] = 0; // обнуляем все кратные ему числа в массиве
		while (num[p + 1] == 0)
			p++; // ищем следующее ненулевое число
		if (p + 1 >= size) { // если выйдем за границы, расширяем массив
			int* tmp = new int[size * 2];
			for (int k = 0; k < size; k++)
				tmp[k] = num[k];

			delete[] num;
			size *= 2;
			num  = tmp;

			for (int l = size / 2; l< size; l++)
				num[l] = l;
			j = 0;
		}
		else {
			//j++;
			prime_numbers.push_back(p + 1); // запоминаем новое простое число
		}
	}
	delete[]num;
	return prime_numbers;
};
vector<vector<int>> kernel_mod2(vector<vector<int>> V, int k) { //находит решения с для сV=0)
	vector < vector <int> > solutions;
	for (int counter = 1; counter < (1 << (k + 1)); counter++) {
		
		int* mask = new int[k + 1];
		for (int i = 0; i < k + 1; i++) {
			mask[i] = 0;
		}
		for (int i = 0; i < k + 1; i++) {
			if (counter & (1 << i))
				mask[i] = 1;
		}

		
		int* result_vector = new int[k];
		for (int i = 0; i < k; i++) {
			result_vector[i] = 0;
		}
		for (int i = 0; i < k + 1; i++) {
			if (mask[i]) {
				for (int j = 0; j < k; j++)
					result_vector[j] += (V[i])[j];
			}
		}

		
		int flag = 0;
		for (int i = 0; i < k; i++) {
			if ((result_vector[i] % 2) != 0)
				flag = 1;
		}
		if (!flag) {
			vector<int> c;
			for (int i = 0; i < k + 1; i++) {
				c.push_back(mask[i]);
			}
			solutions.push_back(c);
		}
	}
	return solutions;
};

int dixon(int n, int k) {
	vector<int> factor_base = create_factor_base(k);
	int t = k + 1;
	int x_i = int(sqrt(n)) + 1;
	vector<int> X;
	vector<vector<int>> V;//матрица
	vector<vector<int>> exponents;//векторы степеней

	while (x_i <= n) {
		while (true) {
			vector<int> vec(k,0);
			int Q = (x_i * x_i) % n;
			vector<int> Q_exponents(k,0);
			for (int j = 0; j < k; j++) {
				int p_j = factor_base[j];
				while (Q % p_j == 0) {
					Q = Q / p_j;
					vec[j] = (vec[j] + 1) % 2;
					Q_exponents[j] = Q_exponents[j] + 1;
				}
				if (Q == 1) {
					X.push_back(x_i);
					V.push_back(vec);
					exponents.push_back(Q_exponents);
					break;
				}
			}
			if (X.size() >= t) {
				break;
			}
			x_i++;
		}
		vector < vector <int> > solutions = kernel_mod2(V, k);
		int size = solutions.size();

		for (int i = 0; i < size; i++) {
			int x = 1;
			int y = 1;
			int* y_exp = new int[k];
			for (int l = 0; l < k; l++) {
				y_exp[l] = 0;
			}
			vector<int>c = solutions[i];

			for (int j = 0; j < k + 1; j++) {
				if (c[j]) {
					x = (x * X[j]) % n;
					for (int s = 0; s < k; s++) {
						y_exp[s] = y_exp[s] + (exponents[j])[s];
					}
				}
			}
			for (int j = 0; j < k; j++) {
				y = (int)(y * pow(factor_base[j], (int)y_exp[j] / 2)) % n;
			}

			if (x != y && x != n - y) {
				int d;
				d = gcd(x + y, n);
				return d;
			}
		}
		x_i++;
	}
	//int d = 0;
	//return d;
};
bool isprime(int n) {
	int s = sqrt(n);
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if (n % primes[i] == 0) {
			return false;
		}
	}
	return true;
};
bool isPower(int n)
{
	int s = sqrt(n);
	vector<int> primes = create_primes(s);
	for (int i = 0; i < primes.size(); i++) {
		if(trunc(log(n) / log(primes[i])) == (log(n) / log(primes[i]))){
			return true;
		}	
	}
	return false;
};
bool checker(char* string)
{
	for (int j = 0, str_len = strlen(string); j < str_len; j++)
	{
		if (string[j] < '0' || string[j] > '9') {
			return false;
		}
	}
	return true;
};

int main()
{
	string filename = "input.txt";
	ifstream input(filename, ios::in);
	string outfilename = "output.txt";
	std::ofstream file(outfilename, std::ios::out);

	if (!input.is_open() || !file.is_open())
		return 0;
	while (!input.eof())
	{
		char nc[100] = { 0 };
		char kc[100] = { 0 };
		char e = '\0';
		int n, k;
		string line;
		getline(input, line);
		if (line[0] == '\r')continue;
		istringstream iss1(line);
		iss1 >> nc;
		iss1 >> kc;
		iss1 >> e;

		if (e != '\0') {
			e = '\0';
			continue;
		}
		if (!checker(nc) || !checker(kc)) continue;
		if (nc[0] == '\0' || kc[0] == '\0') continue;
		n = atoi(nc);
		k = atoi(kc);
		if (n <= 0 || k < 2 || k>=(int)sqrt(n) || isprime(n) || isPower(n)) continue;
		if((n & (int)1) == 0){
			while ((n & (int)1) == 0) {
				n = n >> 1;
			}
			if (n == 1 || isprime(n)|| isPower(n)) continue;
		}
		int d = dixon(n, k);
		file << d << endl;
		
	}
	input.close();
	file.close();
	return 1;
}


